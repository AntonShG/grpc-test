import grpc.SumSaveServiceGrpc;
import grpc.SumSaveServiceImpl;
import grpc.Sumsave;
import io.grpc.ManagedChannel;
import io.grpc.inprocess.InProcessChannelBuilder;
import io.grpc.inprocess.InProcessServerBuilder;
import io.grpc.testing.GrpcCleanupRule;
import org.junit.jupiter.api.*;
import java.io.IOException;
import java.util.Optional;

public class SumSaveServiceImplTest {

    private ManagedChannel channel;
    private long estimatedTime;

    private static final String CODE_11_MESSAGE = "code 11 - out of range";
    private static final String SUCCESSFULLY_MESSAGE = "Sum successfully saved";
    private static final boolean IS_SLOW = true;
    private static final boolean IS_NOT_SLOW = false;
    private static final int OVER_SUM = 1001;
    private static final int NORM_SUM = 1000;
    private static final long TIME_WORK_SERVICE = 5000;

    @BeforeEach
    void setup(TestInfo testInfo) throws IOException {
        GrpcCleanupRule grpcCleanup = new GrpcCleanupRule();
        grpcCleanup.register(InProcessServerBuilder.forName(testInfo.getDisplayName())
                .directExecutor().addService(new SumSaveServiceImpl()).build().start());
        channel = grpcCleanup.register(InProcessChannelBuilder.forName(testInfo.getDisplayName()).directExecutor().build());
    }

    @AfterEach
    void cleanup() {
        Optional.ofNullable(channel).ifPresent(ManagedChannel::shutdownNow);
    }

    @Test
    public void save_overSumFalse_Code11MessageFast() {
        Sumsave.StatusResponse response = getStatusResponseAfterSendRequest(OVER_SUM, IS_NOT_SLOW);
        Assertions.assertEquals(CODE_11_MESSAGE, response.getStatus());
        Assertions.assertTrue(TIME_WORK_SERVICE > estimatedTime);
    }

    @Test
    public void save_normalSumFalse_Code11MessageFast() {
        Sumsave.StatusResponse response = getStatusResponseAfterSendRequest(NORM_SUM, IS_NOT_SLOW);
        Assertions.assertEquals(SUCCESSFULLY_MESSAGE, response.getStatus());
        Assertions.assertTrue(TIME_WORK_SERVICE > estimatedTime);
    }

    @Test
    public void save_overSumAndSlowTrue_Code11MessageSlow() {
        Sumsave.StatusResponse response = getStatusResponseAfterSendRequest(OVER_SUM, IS_SLOW);
        Assertions.assertEquals(CODE_11_MESSAGE, response.getStatus());
        Assertions.assertTrue(TIME_WORK_SERVICE <= estimatedTime);
    }

    @Test
    public void save_normalSumAndSlowTrue_Code11MessageSlow() {
        Sumsave.StatusResponse response = getStatusResponseAfterSendRequest(NORM_SUM, IS_SLOW);
        Assertions.assertEquals(SUCCESSFULLY_MESSAGE, response.getStatus());
        Assertions.assertTrue(TIME_WORK_SERVICE <= estimatedTime);
    }

    private Sumsave.StatusResponse getStatusResponseAfterSendRequest(int sum, boolean isSlow) {
        SumSaveServiceGrpc.SumSaveServiceBlockingStub stub = SumSaveServiceGrpc.newBlockingStub(channel);
        Sumsave.SumRequest request = Sumsave.SumRequest.newBuilder().setSum(sum).setSlow(isSlow).build();
        long startTime = System.currentTimeMillis();
        Sumsave.StatusResponse response = stub.save(request);
        estimatedTime = System.currentTimeMillis() - startTime;
        return response;
    }
}
