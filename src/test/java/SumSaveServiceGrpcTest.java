import grpc.SumSaveServiceGrpc;
import grpc.Sumsave;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.grpcmock.GrpcMock;
import org.grpcmock.definitions.response.Delay;
import org.grpcmock.junit5.GrpcMockExtension;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Optional;

@ExtendWith(GrpcMockExtension.class)
public class SumSaveServiceGrpcTest {

    private ManagedChannel serverChannel;
    private long estimatedTime;

    private static final String CODE_11_MESSAGE = "code 11 - out of range";
    private static final String SUCCESSFULLY_MESSAGE = "Sum successfully saved";
    private static final boolean IS_SLOW = true;
    private static final boolean IS_NOT_SLOW = false;
    private static final int OVER_SUM = 1001;
    private static final int NORM_SUM = 1000;
    private static final long TIME_WORK_SERVICE = 5000;

    @BeforeAll
    static void createServer() {
        GrpcMock.configureFor(GrpcMock.grpcMock(8080).build().start());
    }

    @BeforeEach
    void setupChannel() {
        serverChannel = ManagedChannelBuilder.forAddress("localhost", GrpcMock.getGlobalPort())
                .usePlaintext()
                .build();
    }

    @AfterEach
    void cleanup() {
        Optional.ofNullable(serverChannel).ifPresent(ManagedChannel::shutdownNow);
    }

    @Test
    public void save_overSumFalse_Code11MessageFast() {
        Sumsave.StatusResponse response = getResponseAfterSendRequest(OVER_SUM, IS_NOT_SLOW, CODE_11_MESSAGE);
        Assertions.assertEquals(CODE_11_MESSAGE, response.getStatus());
        Assertions.assertTrue(TIME_WORK_SERVICE > estimatedTime);
    }

    @Test
    public void save_normalSumFalse_Code11MessageFast() {
        Sumsave.StatusResponse response = getResponseAfterSendRequest(NORM_SUM, IS_NOT_SLOW, SUCCESSFULLY_MESSAGE);
        Assertions.assertEquals(SUCCESSFULLY_MESSAGE, response.getStatus());
        System.out.println(estimatedTime);
        Assertions.assertTrue(TIME_WORK_SERVICE > estimatedTime);
    }

    @Test
    public void save_overSumAndSlowTrue_Code11MessageSlow() {
        Sumsave.StatusResponse response = getResponseAfterSendRequest(OVER_SUM, IS_SLOW, CODE_11_MESSAGE);
        Assertions.assertEquals(CODE_11_MESSAGE, response.getStatus());
        Assertions.assertTrue(TIME_WORK_SERVICE <= estimatedTime);
    }

    @Test
    public void save_normalSumAndSlowTrue_Code11MessageSlow() {
        Sumsave.StatusResponse response = getResponseAfterSendRequest(NORM_SUM, IS_SLOW, SUCCESSFULLY_MESSAGE);
        Assertions.assertEquals(SUCCESSFULLY_MESSAGE, response.getStatus());
        Assertions.assertTrue(TIME_WORK_SERVICE <= estimatedTime);
    }

    private Sumsave.StatusResponse getResponseAfterSendRequest(int sum, boolean isSlow, String expectedMessageResponse) {
        Sumsave.StatusResponse response = Sumsave.StatusResponse.newBuilder().setStatus(expectedMessageResponse).build();
        Sumsave.SumRequest request = Sumsave.SumRequest.newBuilder().setSum(sum).setSlow(isSlow).build();

        long del = 0;
        if (isSlow) del = TIME_WORK_SERVICE;
        GrpcMock.stubFor(GrpcMock.unaryMethod(SumSaveServiceGrpc.getSaveMethod())
                .withRequest(request)
                .willReturn(GrpcMock.response(response).withDelay(Delay.fixedDelay(del))));
        SumSaveServiceGrpc.SumSaveServiceBlockingStub serviceStub = SumSaveServiceGrpc.newBlockingStub(serverChannel);

        long startTime = System.currentTimeMillis();
        Sumsave.StatusResponse responseFromService = serviceStub.save(request);
        estimatedTime = System.currentTimeMillis() - startTime;

        return  responseFromService;
    }

}
