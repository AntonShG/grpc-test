package grpc;

import io.grpc.stub.StreamObserver;

public class SumSaveServiceImpl extends SumSaveServiceGrpc.SumSaveServiceImplBase {
    @Override
    public void save(Sumsave.SumRequest request, StreamObserver<Sumsave.StatusResponse> responseObserver) {
        String message = "Sum successfully saved";

        if (request.getSum() > 1000) message = "code 11 - out of range";
        if (request.getSlow()) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Sumsave.StatusResponse response = Sumsave.StatusResponse.newBuilder().setStatus(message).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
